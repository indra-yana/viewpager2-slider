package com.test.vp2slider

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.CompositePageTransformer
import androidx.viewpager2.widget.MarginPageTransformer
import androidx.viewpager2.widget.ViewPager2
import com.test.vp2slider.adapter.SliderAdapter
import com.test.vp2slider.customtransformation.*
import com.test.vp2slider.model.SliderItem
import com.test.vp2slider.model.TransformationItem
import kotlinx.android.synthetic.main.activity_main.*


/****************************************************
 * Created by Indra Muliana (indra.ndra26@gmail.com)
 * On 05/04/2020 16.30
 * Thanks to https://github.com/askNilesh/Viewpager2-Transformation for creating custom Viewpager Transformation
 ****************************************************/
class MainActivity : AppCompatActivity() {

    private val sliderInterval: Long = 5000
    private val sliderItemList = listOf(
        SliderItem("Banner 1", R.drawable.ic_launcher_background),
        SliderItem("Banner 2", R.drawable.ic_launcher_background),
        SliderItem("Banner 3", R.drawable.ic_launcher_background),
        SliderItem("Banner 4", R.drawable.ic_launcher_background)
    )

    private val sliderHandler = Handler()
    private val sliderRunnable = Runnable {
        if (sliderItemList.isNotEmpty()) {
            if (vp2_slider.currentItem + 1 == sliderItemList.size) {
                vp2_slider.currentItem = 0
            } else {
                vp2_slider.currentItem = vp2_slider.currentItem + 1
            }
        }
    }

    private val sliderTransformerList = listOf(
        TransformationItem(
            "Simple Slider Transformation",
            SimpleSliderTransformation()
        ),
        TransformationItem(
            "Scale Transformation",
            CompositePageTransformer().apply {
                addTransformer(MarginPageTransformer(10))
                addTransformer(ScaleTransformation())
            }),
        TransformationItem(
            "Overlap Slider Transformation",
            OverlapSliderTransformation(ViewPager2.ORIENTATION_HORIZONTAL)
        ),
        TransformationItem(
            "ScaleBounce Transformation",
            ScaleBounceTransformation()
        ),
        TransformationItem(
            "CubeInScaling Transformation",
            CubeInScalingTransformation()
        ),
        TransformationItem(
            "CubeOut Rotation Transformation",
            CubeOutRotationTransformation()
        ),
        TransformationItem("Depth Transformation", DepthTransformation()),
        TransformationItem("FadeOut Transformation", FadeOutTransformation()),
        TransformationItem("Fan Transformation", FanTransformation()),
        TransformationItem(
            "FidgetSpin Transformation",
            FidgetSpinTransformation()
        ),
        TransformationItem("Hinge Transformation", HingeTransformation()),
        TransformationItem("Pop Transformation", PopTransformation()),
        TransformationItem(
            "AntiClockSpin Transformation",
            AntiClockSpinTransformation()
        ),
        TransformationItem("Spin Transformation", SpinTransformation()),
        TransformationItem(
            "StackSlider Transformation", StackSliderTransformation(
                ViewPager2.ORIENTATION_HORIZONTAL,
                0.2f,
                0.5f
            )
        ),
        TransformationItem(
            "VerticalFlip Transformation",
            VerticalFlipTransformation()
        ),
        TransformationItem(
            "VerticalShut Transformation",
            VerticalShutTransformation()
        ),
        TransformationItem("ZoomOut Transformation", ZoomOutTransformation())
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupListOfPageTransformer()

        btn_view_all.setOnClickListener {
            Toast.makeText(this@MainActivity, "See all clicked", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onResume() {
        super.onResume()
        sliderHandler.postDelayed(sliderRunnable, sliderInterval)
    }

    override fun onPause() {
        super.onPause()
        sliderHandler.removeCallbacks(sliderRunnable)
    }

    private fun setupListOfPageTransformer() {
        val listOfPageTransformer = arrayListOf<String>()
        for (item in sliderTransformerList) {
            listOfPageTransformer.add(item.title)
        }
        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, listOfPageTransformer)

        transformation_list.adapter = adapter
        transformation_list.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, id: Long) {
                setupViewPager2(position)
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        }
    }

    private fun setupViewPager2(position: Int) {
        val sliderAdapter = SliderAdapter(sliderItemList)
        sliderAdapter.onItemClickListener = object : SliderAdapter.OnItemClickListener {
            override fun onItemClick(view: View?, position: Int) {
                Toast.makeText(
                    this@MainActivity,
                    "Clicked item: ${sliderItemList[position].title}",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        vp2_slider.apply {
            adapter = sliderAdapter
            clipChildren = false
            clipToPadding = false
            offscreenPageLimit = 3

            getChildAt(0).overScrollMode = RecyclerView.OVER_SCROLL_NEVER
            setPageTransformer(sliderTransformerList[position].transformer)
            registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    sliderHandler.removeCallbacks(sliderRunnable)
                    sliderHandler.postDelayed(sliderRunnable, sliderInterval)
                }
            })
        }

        dots_indicator.setViewPager2(vp2_slider)
    }


}
