package com.test.vp2slider.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.test.vp2slider.R
import com.test.vp2slider.model.SliderItem
import kotlinx.android.synthetic.main.item_slider.view.*

/****************************************************
 * Created by Indra Muliana (indra.ndra26@gmail.com)
 * On 05/04/2020 16.30
 ****************************************************/
class SliderAdapter(private var sliderItem: List<SliderItem>) : RecyclerView.Adapter<SliderAdapter.SliderViewHolder>() {

    var onItemClickListener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SliderViewHolder {
        return SliderViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_slider,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: SliderViewHolder, position: Int) {
        holder.bindItem(sliderItem[position])
    }

    override fun getItemCount(): Int {
        return sliderItem.size
    }

    inner class SliderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        fun bindItem(sliderItem: SliderItem) {
            itemView.tv_item_title.text = sliderItem.title
            itemView.iv_item_banner.setImageResource(sliderItem.imageResource)

            itemView.card_view_item.setOnClickListener(this)
        }

        override fun onClick(p0: View?) {
            onItemClickListener?.onItemClick(p0, bindingAdapterPosition)
        }
    }

    interface OnItemClickListener {
        fun onItemClick(view: View?, position: Int)
    }
}