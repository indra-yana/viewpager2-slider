package com.test.vp2slider.model

/****************************************************
 * Created by Indra Muliana (indra.ndra26@gmail.com)
 * On 05/04/2020 16.29
 ****************************************************/
data class SliderItem(
    var title: String,
    var imageResource: Int
)